<?php

namespace App\Shapes;


interface ShapeInterface
{
    //Available Sizes
    const S = 5;
    const M = 7;
    const L = 11;

    const PREFIX_CHAR = '+';
    const BODY_CHAR = 'x';

    public function getName(): string;

    public function getSize(): int;

    public function render(): string;

    public function setSize(int $size);

    public function setRandomSize();

}
<?php

namespace App\Shapes;

class Star extends Shape implements ShapeInterface
{

    const NAME = 'Star';

    //I couldn't find equation for stars
    protected static $stars = [
        self::S => <<<'STAR'
    +
    x
 +xxxxx+
    x
    +
STAR
         ,
         self::M => <<<'STAR'
      +
      x
    xxxxx    
 +xxxxxxxxx+    
    xxxxx
      x
      +
STAR
         ,
         self::L => <<<'STAR'
         +
         x
        xxx
      xxxxxxx
    xxxxxxxxxxx
 +xxxxxxxxxxxxxxx+
    xxxxxxxxxxx
      xxxxxxx
        xxx
         x
         +
STAR
    ];

    public function render(): string
    {
        return self::$stars[$this->getSize()];
    }
}
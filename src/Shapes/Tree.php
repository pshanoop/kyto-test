<?php

namespace App\Shapes;

use App\Helpers\Maths;

class Tree extends Shape implements ShapeInterface
{

    const NAME = 'Tree';

    public function render(): string
    {
        return $this->renderTree();
    }

    private function renderTree():string
    {
        $renderStr = '';
        $spaceLen = floor(Maths::nThOdd($this->getSize()) / 2);

        for ($i = 0; $i < $this->getSize(); $i++) {
            if ($i == 0) {
                $renderStr .= str_repeat(' ', $spaceLen) . '+' . PHP_EOL;
                continue;
            }

            $renderStr .= str_repeat(' ', $spaceLen--);
            $renderStr .= str_repeat('x', Maths::nThOdd($i)) . PHP_EOL;
        }

        return $renderStr;

    }
}
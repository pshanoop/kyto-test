<?php

namespace App\Shapes;


class Renderer
{

    protected $shape;

    /**
     * Renderer constructor.
     * @param string $shape
     * @param int $size
     * @throws ShapeNotFoundException
     */
    public function __construct(string $shape, int $size = null)
    {
        switch ($shape) {
            case Star::NAME:
                $this->shape = new Star($size);
                break;
            case Tree::NAME:
                $this->shape = new Tree($size);
                break;
            default:
                throw new ShapeNotFoundException('Shape not found');
        }

        if (!isset($size)) {
            $this->shape->setRandomSize();
        }

    }

    public static function getAvailableShapes()
    {
        return [
            Star::NAME,
            Tree::NAME
        ];
    }

    public function render()
    {
        return $this->shape->render();
    }

}
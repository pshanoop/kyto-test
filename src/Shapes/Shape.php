<?php

namespace App\Shapes;


abstract class Shape implements ShapeInterface
{
    protected $availableSizes = [self::S, self::M, self::L];

    protected $size;

    const NAME ='';

    public function __construct(int $size = null)
    {
        $this->size = $size;
    }

    public function getName(): string
    {
        self::NAME;
    }

    public function getSize(): int
    {
        return $this->size;
    }


    /**
     * @param mixed $size
     * @return Shape
     */
    public function setSize(int $size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return $this
     */
    public function setRandomSize()
    {
        $this->size = $this->availableSizes[mt_rand(0, 2)];

        return $this;
    }
}
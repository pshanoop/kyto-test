<?php


namespace App\Commands;


use App\Shapes\Renderer as ShapeRenderer;
use App\Shapes\ShapeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Shape extends Command
{

    protected $sizes = [
        'S' => ShapeInterface::S,
        'M' => ShapeInterface::M,
        'L' => ShapeInterface::L
    ];

    protected function configure()
    {
        $availableShapes = implode('|', ShapeRenderer::getAvailableShapes());
        $this
            ->setName('shape')
            ->setDescription('Shape Ascii Generator')
            ->addArgument('name', InputArgument::REQUIRED,
                sprintf('Name of the shapes [%s]', $availableShapes))
            ->addOption('size', 's', InputArgument::OPTIONAL, 'Size of the shape [S|M|L]');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $shapeName = $input->getArgument('name');
        $size = $this->sizes[$input->getOption('size')] ?? null;
        $shape = new ShapeRenderer($shapeName, $size);
        $output->writeln($shape->render());
    }


}
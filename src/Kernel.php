<?php

namespace App;


use App\Shapes\ShapeInterface;
use App\Shapes\ShapeNotFoundException;
use \Klein\Klein;
use App\Shapes\Renderer;
use Symfony\Component\Console\Application as CliApplication;
use App\Commands\Shape as ShapeCommand;


class Kernel
{

    /** @var Klein $router */
    protected $router;

    protected $cliHandler;

    protected $isCli;

    private function __construct()
    {

        $this->isCli = (php_sapi_name() == 'cli');

        if ($this->isCli) {
            $this->cliHandler = new CliApplication();
        } else {
            $this->router = new Klein();
        }
    }


    public static function create()
    {
        return new Kernel();
    }

    public function run()
    {
        if ($this->isCli) {
            $this->handleCli();

        } else {
            $this->addRoute();
            $this->router->onHttpError($this->httpErrorHandler());
            $this->router->dispatch();
        }
    }

    private function handleCli()
    {
        $this->cliHandler->add(new ShapeCommand());
        $this->cliHandler->run();
    }

    private function addRoute()
    {
        $this->router->respond('GET', '/[a:shape]/[S|M|L:size]?', $this->htmlResponse());
    }

    private function htmlResponse()
    {
        return function ($request, $response) {
            $sizes = [
                'S' => ShapeInterface::S,
                'M' => ShapeInterface::M,
                'L' => ShapeInterface::L
            ];

            $size = $sizes[$request->size] ?? null;

            /**@var \Klein\Response $response */;
            try {

                $shape = new Renderer($request->shape, $size);
                $response
                    ->body(sprintf('<html><body><pre>%s</pre></body></html>', $shape->render()));
            } catch (ShapeNotFoundException $exception) {
                $response
                    ->body(sprintf('<html><body><p>%s</p></body></html>', $exception->getMessage()));
                $response->code(404);
            }
            $response->send();
        };
    }

    private function httpErrorHandler()
    {
        return function ($code, $router) {
            /** @var Klein $router */
            if ($code >= 400 && $code < 500) {
                $router->response()->body(
                    'Oops, routing error with code: '. $code
                );
            } elseif ($code >= 500 && $code <= 599) {
                error_log('Oops, Server just died.');
            }
        };

    }
}
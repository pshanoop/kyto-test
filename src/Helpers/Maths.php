<?php

namespace App\Helpers;


class Maths
{

    /**
     * Returns n-th odd number. Where N is positive integer
     * @param int $n
     * @return int
     */
    public static function nThOdd(int $n):int
    {
        return 1 + 2 * ($n - 1);
    }

}
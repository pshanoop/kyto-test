###Getting started.

`composer install`


####Console commands  

 ```
 php bin/console shape Star 
 php bin/console shape Tree  
 php bin/console shape Star -s S
 ```
 Size can be given with option `--size`
 
 Size values can be `S`,`M`,`L`.
 
 ####Starting web server to get in browser
 
 Using  simple php server.
  
 ` php -S localhost:8080 -t public `
 
 Available routes.
 [Star](http://localhost:8080/Star)
 
 [Small-Star](http://localhost:8080/Star/S)
 
 [Medium-Star](http://localhost:8080/Star/M)
 
 [Large-Star](http://localhost:8080/Star/L)
 
 [Tree](http://localhost:8080/Tree)
 
 [Small-Tree](http://localhost:8080/Tree/S)
 
 [Medium-Tree](http://localhost:8080/Tree/M)
 
 [Large-Tree](http://localhost:8080/Tree/L)
 
 Here `S`,`M` and `L` are sizes.
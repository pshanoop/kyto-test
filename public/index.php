<?php


require __DIR__. '/../vendor/autoload.php';

use App\Kernel;

$app = Kernel::create();

$app->run();